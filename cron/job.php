<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../configs/config.php';
$app = new Silex\Application();
/**
 * Loads the database provider and enables us to use the database!
 * Must be included after $app has been initialized.
 */
require_once __DIR__.'/../provider/database.config.php';


$sql = 'SELECT * FROM jobs WHERE status = "QUEUED"';
$sources = $app['db']->query($sql);
$total_pages = 0;
$total_sources = count($sources);
while($source = $sources->fetch()) {
    $total_images = 0;
    $url = parse_url($source['url']);

    //check so that another instance haven't taken the job
    $result = $app['db']->query('SELECT * FROM jobs WHERE id = ' . $source['id']);
    $result = $result->fetch();
    if($result['status'] == 'QUEUED') {
        $app['db']->update('jobs', array('status' => 'WORKING'), array('id' => $source['id']));
    } else {
        // Jump to next source
        continue;
    }

    for($n = 1; ;$n++) {
        $total_pages += 1;
        $pagination = strpos($source['url'], '?') === false ? '/?paged=' : '&paged=';
        $current_url = $n != 1 ? (rtrim($source['url'], '/') . $pagination . $n) : $source['url'];
        $rss = null;
        try {
            $rss = new SimpleXMLElement(file_get_contents($current_url));
        } catch(Exception $e) {
            $app['db']->update('jobs', array('status' => 'FINISHING', 'total_images' => $total_images), array('id' => $source['id']));
            break 1;
        }

        if(!isset($rss->channel->item) || empty($rss->channel->item)) {
            $app['db']->update('jobs', array('status' => 'FINISHING', 'total_images' => $total_images), array('id' => $source['id']));
            break 1;
        }

        foreach($rss->channel->item as $item) {
            $dir = 'data/' . $url['host'] . '/'. date('Y', strtotime($item->pubDate)) . '/' . date('F', strtotime($item->pubDate));
            $doc = new DOMDocument();
            @$doc->loadHTML('<?xml encoding="UTF-8">' . $item->children("content", true));
            $images = $doc->getElementsByTagName('img');
            if(!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
            foreach($images as $image) {
                $total_images += 1;
                file_put_contents($dir . '/' . getFileName($image->getAttribute('src')), file_get_contents($image->getAttribute('src')));
            }
        }
    }

    // Create zipfile for the current url
    $path = 'data/'.$url['host'];
    $zip = $url['host'] . uniqid() . "-" . date('Ymd') . ".zip";
    HZip::zipDir($path, (_PUBLIC_DIR.$zip));
    if(file_exists(_PUBLIC_DIR.$zip)) {
        $app['db']->update('jobs', array('status' => 'DONE', 'filename' => $zip), array('id' => $source['id']));
        system("rm -r ".escapeshellarg($path));

        // Mail the downloader if they have registered their email!
        $result = $app['db']->query('SELECT * FROM emails WHERE job_id = '. $source['id']);
        $user = $result->fetch();

        $subject = 'wp-images.org - your download is ready!';
        $message = 'Your download is complete, go to <a href="http://wp-images.org/data/' . $zip . '">this link to download</a>.';
        $message .= "<br />You have 1 hour to download it before the file is deleted from our servers. Then you'll have repeat the process!";
        $message .= "<br />Thank you for using our service, please share it on twitter, facebook or send a link to your friends :)";
        $message .= "<br /><br />--<br />Best regards, skynet@wp-images.org";

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers
        $headers .= 'To: <' . $user['email'] .'>' . "\r\n";
        $headers .= 'From: Download reminder <skynet@wp-images.org>' . "\r\n";
        mail($user['email'], $subject, $message, $headers);

    } else {
        $app['db']->update('jobs', array('status' => 'ERROR'), array('id' => $source['id']));
        system("rm -r ".escapeshellarg($path));
    }

}

echo "Job done, images downloaded smoothly.\n\nTotal images: $total_images\nTotal pages: $total_pages\nTotal sources: $total_sources\n\n";

function getFileName($path) {
    $path = pathinfo($path);
    return $path['filename'] . '.' . $path['extension'];
}


class HZip
{
    /**
     * Add files and sub-directories in a folder to zip file.
     * @param string $folder
     * @param ZipArchive $zipFile
     * @param int $exclusiveLength Number of text to be exclusived from the file path.
     */
    private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..') {
                $filePath = "$folder/$f";
                // Remove prefix from file path before add to zip.
                $localPath = substr($filePath, $exclusiveLength);
                if (is_file($filePath)) {
                    $zipFile->addFile($filePath, $localPath);
                } elseif (is_dir($filePath)) {
                    // Add sub-directory.
                    $zipFile->addEmptyDir($localPath);
                    self::folderToZip($filePath, $zipFile, $exclusiveLength);
                }
            }
        }
        closedir($handle);
    }

    /**
     * Zip a folder (include itself).
     * Usage:
     *   HZip::zipDir('/path/to/sourceDir', '/path/to/out.zip');
     *
     * @param string $sourcePath Path of directory to be zip.
     * @param string $outZipPath Path of output zip file.
     */
    public static function zipDir($sourcePath, $outZipPath)
    {
        $pathInfo = pathInfo($sourcePath);
        $parentPath = $pathInfo['dirname'];
        $dirName = $pathInfo['basename'];

        $z = new ZipArchive();
        $z->open($outZipPath, ZIPARCHIVE::CREATE);
        $z->addEmptyDir($dirName);
        self::folderToZip($sourcePath, $z, strlen("$parentPath/"));
        $z->close();
    }
}
<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../configs/config.php';
$app = new Silex\Application();
/**
 * Loads the database provider and enables us to use the database!
 * Must be included after $app has been initialized.
 */
require_once __DIR__.'/../provider/database.config.php';

$sql = "SELECT * FROM jobs WHERE status = 'DONE' AND updated < (NOW() - INTERVAL 1 HOUR)";
$result = $app['db']->query($sql);
while($row = $result->fetch()) {
    if(!empty($row['filename'])) {
        $path = __DIR__.'/../public/data/'.$row['filename'];
        if(file_exists($path)) {
            echo "\nRemoving " . $path . "\n";
            system("rm -r ".escapeshellarg($path));
        }
    }
}

$sql = "DELETE FROM jobs WHERE status = 'DONE' AND updated < (NOW() - INTERVAL 1 HOUR)";
$deleted = $app['db']->query($sql);

if($deleted) {
    echo "Deletion successfull.\n\n";
}

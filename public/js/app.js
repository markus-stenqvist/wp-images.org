$(function() {

    var current_id;
    var handle;

    $('#download').click(function(event) {
        event.preventDefault();
        var btn = $(this);
        if(btn.attr('disabled') != 'disabled') {
            var url = $('.textbox').val();
            if(url.length > 4) {
                $('.dl-info').remove();
                $.post('/dl', {url: url}, function(json) {
                    current_id = json.id;
                    handle = setInterval(check, 5000);
                    btn.attr('disabled', 'disabled');
                    $('.dl-container').prepend('<div class="alert alert-info dl-info"><i class="icon-spinner icon-spin icon-large"></i> <strong>The download process has begun!</strong> This may take a while, don\'t close this window..' +
                        '<span id="mailme"><br />If you want we can email you once the images are ready for download:<br /><input type="text" placeholder="my@email.com" id="email" /> <a class="btn btn-primary" id="submit-email" href="#">Email me</a></span></div>');
                });
            } else {
                alert("That is not a valid url!");
            }
        } else {
            alert("There is already a download in progress!");
        }
    });

    var check = function() {
        $.getJSON('/dl/' + current_id, function(json) {
            if(json.status === 'DONE') {
                clearInterval(handle);
                $('.dl-info').remove();
                var images = 0;
                if(json.total_images != null) {
                    images = json.total_images;
                }
                $('.dl-container').prepend('<div class="alert alert-success dl-info"><strong>The download is complete!</strong> <a href="/data/'+ json.filename +'" download="'+ json.filename +'">Click here to download!</a>' +
                    '<br />Total images: ' + images + ' - Thanks for using our service!');

            } else if(json.status === 'FINISHING') {
                var images = 0;
                if(json.total_images != null) {
                    images = json.total_images;
                }
                $('.dl-info').remove();
                $('.dl-container').prepend('<div class="alert alert-warning dl-info"><i class="icon-spinner icon-spin icon-large"></i> <strong>The download is soon finished!</strong> Just hang on a little while longer, and the download will soon be ready.' +
                    '<br />We found a total of ' + images+ ' images!');
            } else if(json.status == 'ERROR') {
                $('.dl-info').remove();
                $('.dl-container').prepend('<div class="alert alert-danger dl-info"><strong>An error occurred during the download process.</strong> Did you really input a URL to a RSS-feed?');
            }
        });
    };

    $(document).on('click', '#submit-email', function(event) {
        event.preventDefault();
        var email = $('#email').val();
        if(email.length > 4) {
            $.post('/mailme', {email: email, id: current_id}, function(json) {
                if(json.mailme == true) {
                    $('#mailme').remove();
                    alert("Your email has been registered, you will be mailed with a download link.\nDon't forget to check spam mail!");
                } else {
                    alert("Couldn't register your email, please try again or stay on this page!");
                }
            });
        } else {
            alert("That's not a valid email!");
        }
    });

});
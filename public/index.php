<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../configs/config.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

/**
 * Loads the database provider and enables us to use the database!
 * Must be included after $app has been initialized.
 */
require_once __DIR__.'/../provider/database.config.php';
require_once __DIR__.'/../provider/twig.php';

if(_DEBUG == true) {
    $app['debug'] = true;
}

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html', array(
        'name' => 'Markus',
    ));
});

$app->post('/dl', function(Request $request) use ($app) {
    $url = $request->get('url');
    if(strlen($url) < 4) {
        return $app->json(array('error' => true));
    }
    $url = strpos($url, "http://") !== false ? $url: ('http://' . $url);
    $ip = $request->getClientIp();
    $datetime = date('Y-m-d H:i:s');
    $result = $app['db']->insert('jobs', array('url' => $url, 'status' => 'QUEUED', 'requested_by' => $ip, 'created' => $datetime, 'updated' => $datetime));
    if($result) {
        return $app->json($app['db']->fetchAssoc('SELECT * FROM jobs ORDER BY id DESC LIMIT 1'));
    }
    return $app->json($result);
});

$app->get('/dl/{id}', function($id) use ($app) {
    $sql = "SELECT * FROM jobs WHERE id = ?";
    $result = $app['db']->fetchAssoc($sql, array((int) $id));
    return $app->json($result);
});

$app->post('/mailme', function(Request $request) use ($app) {
    $email = $request->get('email');
    $job_id = $request->get('id');
    $ip = $request->getClientIp();
    $result = $app['db']->insert('emails', array('job_id' => $job_id, 'email' => $email, 'ip' => $ip, 'created' => date('Y-m-d H:i:s')));
    if($result) {
        return $app->json(array('mailme' => true));
    }
    return $app->json(array('mailme' => false));
});

$app->run();